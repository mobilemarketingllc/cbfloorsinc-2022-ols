var $ = jQuery;
 jQuery( document ).ready(function() {
    jQuery('.searchIcon .fl-icon').click(function(){
        jQuery('.searchModule').slideToggle();
    });    

    jQuery(document).mouseup(function(e) {
        var container = jQuery(".searchModule, .searchIcon");
        // if the target of the click isn't the container nor a descendant of the container
        if (!container.is(e.target) && container.has(e.target).length === 0 && jQuery('.searchModule').css('display') !=='none') 
        {
            jQuery('.searchModule').slideToggle();
        }
    });
});

function validateChar(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 123)) {
        if (charCode == 8 || charCode == 32 || charCode == 9){
        console.log(charCode);
        return true;
        }
        else
        return false;
    } else
        return true;
}
function validateNum(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        if (charCode == 43 || charCode == 40 || charCode == 41 || charCode == 9)
        return true;
        else
        return false;
    } else
        return true;
}

function ValidateEmail(mail) {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        return (false);
    }                    
    return (true);
}