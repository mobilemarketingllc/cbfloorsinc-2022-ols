import React from "react";

export default function BrandFacet({ handleFilterClick, productBrands }) {
  return (
    <div class="facet-wrap facet-display">
      <strong>Brand</strong>
      <div className="facetwp-facet">
        {Object.keys(productBrands).map((brand, i) => {
          if (brand && productBrands[brand] > 0) {
            return (
              <div>
                <span
                  id={`brand-filter-${i}`}
                  key={i}
                  data-value={`${brand.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("brand_facet", e.target.dataset.value)
                  }>
                  {" "}
                  {brand} {` (${productBrands[brand]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
