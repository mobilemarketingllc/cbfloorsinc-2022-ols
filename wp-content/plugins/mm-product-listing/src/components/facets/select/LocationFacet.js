import React from "react";

export default function LocationFacet({
  handleFilterChange,
  productLocations,
}) {
  const handleLocationChange = (e) => {
    const selectedLocation = e.target.value;
    handleFilterChange("location_facet", selectedLocation);
  };

  return (
    <div className="facet-wrap facet-display">
      <div className="facetwp-facet">
        <select onChange={handleLocationChange}>
          <option value="">CHOOSE LOCATION</option>
          {Object.keys(productLocations).length > 0 &&
            Object.keys(productLocations).map((location, i) => {
              if (location && productLocations[location] > 0) {
                return (
                  <option key={i} value={location.toLowerCase()}>
                    {location} ({productLocations[location]})
                  </option>
                );
              }
              return null; // Skip locations with no products
            })}
        </select>
      </div>
    </div>
  );
}
