import React from "react";

export default function ThicknessFacet({
  handleFilterClick,
  productThickness,
}) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productThickness = sortObject(productThickness);

  return (
    <div class="facet-wrap facet-display">
      <strong>Thickness</strong>
      <div className="facetwp-facet">
        {Object.keys(productThickness).map((thickness, i) => {
          if (thickness && productThickness[thickness] > 0) {
            return (
              <div>
                <span
                  id={`thickness-filter-${i}`}
                  key={i}
                  data-value={`${thickness.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("thickness_facet", e.target.dataset.value)
                  }>
                  {" "}
                  {thickness} {` (${productThickness[thickness]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
